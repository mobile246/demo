//
//  UIImageView.swift
//  Wisdomvast_Demo
//
//  Created by Kitja on 10/30/23.
//

import UIKit

extension UIImageView {
    
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self?.image = UIImage(systemName: "photo.on.rectangle")
                }
            }
        }
    }
}
