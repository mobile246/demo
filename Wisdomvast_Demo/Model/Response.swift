//
//  Response.swift
//  Wisdomvast_Demo
//
//  Created by Kitja on 10/28/23.
//

import Foundation

// MARK: - Welcome
struct ResponseModel: Codable {
    let code: Int
    let message: String
    let responseObject: ResponseObject
}

// MARK: - ResponseObject
struct ResponseObject: Codable {
    let contentShelfs: [ContentShelf]
}

// MARK: - ContentShelf
struct ContentShelf: Codable {
    let items: [Item]?
    let subTitle, title, type: String
    let viewMore: ViewMore
    let coverURL: String?

    enum CodingKeys: String, CodingKey {
        case items, subTitle, title, type, viewMore
        case coverURL = "coverUrl"
    }
}

// MARK: - Item
struct Item: Codable {
    let coverURL: String
    let id: Int
    let subTitle, title, type, album: String?
    let artist: String?
    let conditions: Int?
    let contentURL, numOfListener: String?
    let numOfSong, statusTag: Int?

    enum CodingKeys: String, CodingKey {
        case coverURL = "coverUrl"
        case id, subTitle, title, type, album, artist, conditions
        case contentURL = "contentUrl"
        case numOfListener, numOfSong, statusTag
    }
}

// MARK: - ViewMore
struct ViewMore: Codable {
    let contentURL: String
    let enabled: Bool

    enum CodingKeys: String, CodingKey {
        case contentURL = "contentUrl"
        case enabled
    }
}
