//
//  NetworkingService.swift
//  Wisdomvast_Demo
//
//  Created by Kitja on 10/30/23.
//

import Foundation

final class NetworkingService {
    static let shared = NetworkingService()
  
    func fetchData<T>(
        with request: URLRequest,
        responseType: T.Type,
        completionHandler: @escaping(Result<T, Error>) -> Void
    ) where T: Codable {
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                completionHandler(.failure(NetworkingError.unknow(error!)))
                return
            }
            guard (response as? HTTPURLResponse)?.statusCode == 200 else {
                completionHandler(.failure(NetworkingError.serverError))
                return
            }
            guard let data = data else {
                completionHandler(.failure(NetworkingError.invalidData))
                return
            }
            do {
                let response = try JSONDecoder().decode(T.self, from: data)
                completionHandler(.success(response))
            } catch {
                completionHandler(.failure(NetworkingError.invalidData))
            }
        }
        task.resume()
    }
}

enum NetworkingError: Error, LocalizedError {
    case unknow(Error)
    case serverError
    case invalidData
    case apiError(String)
    
    var errorDescription: String? {
        switch self {
        case .unknow(let error):
            return "Unknow Error \(error)"
        case .serverError:
            return "No Internet Connection"
        case .invalidData:
            return "Invalid Data"
        case .apiError(let message):
            return "Sever Error \(message)"
        }
    }
}
