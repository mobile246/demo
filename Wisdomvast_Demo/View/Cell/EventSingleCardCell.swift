//
//  EventSingleCardCell.swift
//  Wisdomvast_Demo
//
//  Created by Kitja on 10/28/23.
//

import UIKit

class EventSingleCardCell: UITableViewCell {
    static let identifier = "EventSingleCardCell"
    
    private let cardView: UIView = {
        let cardView = UIView()
        cardView.translatesAutoresizingMaskIntoConstraints = false
        cardView.layer.cornerRadius = 16
        cardView.layer.borderWidth = 1
        cardView.layer.borderColor = UIColor.systemGray.cgColor
        return cardView
    }()
    
    private var imageCardView: UIImageView = {
        let imageCardView = UIImageView()
        imageCardView.image = UIImage(systemName: "photo")
        imageCardView.contentMode = .scaleToFill
        imageCardView.layer.masksToBounds = true
        imageCardView.layer.cornerRadius = 16
        imageCardView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        imageCardView.translatesAutoresizingMaskIntoConstraints = false
        return imageCardView
    }()
    
    private var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    
    private let detailLabel: UILabel = {
        let detailLabel = UILabel()
        detailLabel.textAlignment = .center
        detailLabel.textColor = .systemGray
        detailLabel.numberOfLines = 0
        detailLabel.translatesAutoresizingMaskIntoConstraints = false
        return detailLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.layer.cornerRadius = 12
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func setupUI() {
        contentView.addSubview(cardView)
        contentView.addSubview(imageCardView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(detailLabel)
        
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: contentView.topAnchor),
            cardView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            cardView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),
            cardView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            imageCardView.topAnchor.constraint(equalTo: cardView.topAnchor),
            imageCardView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor),
            imageCardView.trailingAnchor.constraint(equalTo: cardView.trailingAnchor),
            imageCardView.heightAnchor.constraint(equalToConstant: 180),
            
            titleLabel.topAnchor.constraint(equalTo: imageCardView.bottomAnchor, constant: 16),
            titleLabel.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 24),
            titleLabel.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -24),
            
            detailLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16),
            detailLabel.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 24),
            detailLabel.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -24)
        ])
    }
    
    func confiure(data: ContentShelf) {
        titleLabel.text = data.title
        detailLabel.text = data.subTitle
        
        let url = URL(string: data.coverURL!)!
        imageCardView.load(url: url)
    }
}
