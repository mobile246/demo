//
//  CarouselCellCollectionViewCell.swift
//  Wisdomvast_Demo
//
//  Created by Kitja on 10/28/23.
//

import UIKit

class CarouselCellCollectionViewCell: UICollectionViewCell {
    static let identifier = "CarouselCellCollectionViewCell"
    
    private lazy var image: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(image)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        image.frame = contentView.bounds
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(imageUrl: String) {
        if let url = URL(string: imageUrl) {
            image.load(url: url)
        } 
    }
}
