//
//  ViewController.swift
//  Wisdomvast_Demo
//
//  Created by Kitja on 10/27/23.
//

import UIKit
import Combine

class ViewController: UIViewController {
    private let viewModel = ViewModel()
    private let input = PassthroughSubject<ViewModel.Input, Never>.init()
    private var cancellable = Set<AnyCancellable>.init()
    private var response: ResponseObject?
    private lazy var isDarkMode = CurrentValueSubject<Bool, Never>.init( self.traitCollection.userInterfaceStyle == .dark)
    
    // MARK: - UI Components
    private var imageSliderView: BannerCell = {
        let imageSliderView = BannerCell()
        imageSliderView.translatesAutoresizingMaskIntoConstraints = false
        return imageSliderView
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.frame = view.bounds
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.addSubview(pullControl)
        tableView.register(CarouselCell.self, forCellReuseIdentifier: CarouselCell.identifier)
        tableView.register(BannerCell.self, forCellReuseIdentifier: BannerCell.identifier)
        tableView.register(EventSingleCardCell.self, forCellReuseIdentifier: EventSingleCardCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    private lazy var activityindicator: UIActivityIndicatorView = {
        let activityindicator = UIActivityIndicatorView()
        activityindicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityindicator.center = self.view.center
        activityindicator.backgroundColor = .systemGray5
        activityindicator.clipsToBounds = true
        activityindicator.layer.cornerRadius = 10
        activityindicator.style = .large
        return activityindicator
    }()
    
    private lazy var pullControl: UIRefreshControl = {
        let pullControl = UIRefreshControl()
        pullControl.addTarget(self, action: #selector(refreshListData), for: .valueChanged)
        return pullControl
    }()
    
    private lazy var toggleDarkmodeBtn: UIButton = {
        let toggleDarkmodeBtn = UIButton()
        toggleDarkmodeBtn.addTarget(self, action: #selector(toggleDarkMode), for: .touchUpInside)
        toggleDarkmodeBtn.setImage(UIImage(named: "Image"), for: .normal)
        toggleDarkmodeBtn.translatesAutoresizingMaskIntoConstraints = false
        return toggleDarkmodeBtn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBinder()
        input.send(.viewDidLoad)
    }
    
    override func viewDidLayoutSubviews() {
        setupUI()
    }
    
    // MARK: - Setup UI
    private func setupUI() {
        view.addSubview(tableView)
        view.addSubview(activityindicator)
        view.addSubview(toggleDarkmodeBtn)
        setupNavigationBar(title: "WISDOM")
        setupConstraints()
        setuptoggleDarkmodeBtnUI()
    }
    
    private func setupNavigationBar(title: String) {
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = .systemIndigo
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        navigationItem.title = title
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            toggleDarkmodeBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -38),
            toggleDarkmodeBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -38),
            toggleDarkmodeBtn.widthAnchor.constraint(equalToConstant: 56),
            toggleDarkmodeBtn.heightAnchor.constraint(equalToConstant: 56)
        ])
    }
    
    private func setuptoggleDarkmodeBtnUI() {
        toggleDarkmodeBtn.layer.cornerRadius = toggleDarkmodeBtn.layer.bounds.width / 2
        toggleDarkmodeBtn.clipsToBounds = true
        toggleDarkmodeBtn.layer.shadowPath = UIBezierPath(roundedRect: toggleDarkmodeBtn.bounds, cornerRadius: toggleDarkmodeBtn.layer.cornerRadius).cgPath
        toggleDarkmodeBtn.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        toggleDarkmodeBtn.layer.shadowOpacity = 0.5
        toggleDarkmodeBtn.layer.masksToBounds = false
        
        isDarkMode
            .sink { [weak self] isDark in
                if isDark {
                    self?.toggleDarkmodeBtn.backgroundColor = .gray
                    self?.toggleDarkmodeBtn.tintColor = .white
                } else {
                    self?.toggleDarkmodeBtn.backgroundColor = .white
                    self?.toggleDarkmodeBtn.tintColor = .black
                }
            }
            .store(in: &cancellable)
    }
    
    // MARK: - Setup Binder
    private func setupBinder() {
        viewModel.tranfrom(input: input)
            .receive(on: DispatchQueue.main)
            .sink { [weak self]  event in
                guard let self = self else { return }
                switch event {
                case .startLoading:
                    activityindicator.startAnimating()
                case .fetchDataSuccess(let response):
                    self.response = response
                    setupComponent()
                case .fetchDataError(let error):
                    errorHandle(message: error.localizedDescription)
                }
            }
            .store(in: &cancellable)
    }
    
    // MARK: - Setup Compenents
    private func setupComponent() {
        activityindicator.stopAnimating()
        pullControl.endRefreshing()
        tableView.reloadData()
    }
    
    // MARK: - Error Handle
    private func errorHandle(message: String) {
        activityindicator.stopAnimating()
        pullControl.endRefreshing()
        let dismissAction = UIAlertAction(title: "OK", style: .default) { _ in}
        let retryAction = UIAlertAction(title: "Retry", style: .default) { [weak self] _ in
            self?.refreshListData()
        }
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(dismissAction)
        alert.addAction(retryAction)
        self.present(alert, animated: true)
    }
    
    // MARK: - RefreshData
    @objc private func refreshListData() {
        input.send(.retryRequest)
    }
    
    // MARK: - Action
    @objc private func toggleDarkMode() {
        overrideUserInterfaceStyle = traitCollection.userInterfaceStyle == .light ? .dark : .light
        isDarkMode.send(overrideUserInterfaceStyle == .light ? false : true)
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            guard let bannerCell = tableView.dequeueReusableCell(withIdentifier: BannerCell.identifier) as? BannerCell else {
                return UITableViewCell()
            }
            
            if let banner = response?.contentShelfs.filter({$0.type == "banner_1"}).first?.items {
                bannerCell.configure(itemList: banner)
            }
            return bannerCell
        }
        
        if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CarouselCell.identifier) as? CarouselCell else {
                return UITableViewCell()
            }
            if let item = response?.contentShelfs.filter({$0.type == "carousel"}).first?.items {
                cell.configure(data: item)
            }
            return cell
        }
        
        if indexPath.section == 2 {
            guard let eventSingleCardCell = tableView.dequeueReusableCell(withIdentifier: EventSingleCardCell.identifier) as? EventSingleCardCell else {
                return UITableViewCell()
            }
            if let item = response?.contentShelfs.filter({$0.type == "eventSingleCard"}).first {
                eventSingleCardCell.confiure(data: item)
            }
            return eventSingleCardCell
        }
        
        if indexPath.section == 3 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CarouselCell.identifier) as? CarouselCell else {
                return UITableViewCell()
            }
            if let item = response?.contentShelfs.filter({$0.type == "carousel"}).last?.items {
                cell.configure(data: item)
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 200
        } else if indexPath.section == 2 {
            return  376
        } else {
            return 150
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        response?.contentShelfs.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section != 0 {
            return "Header"
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}
