//
//  ViewModel.swift
//  Wisdomvast_Demo
//
//  Created by Kitja on 10/28/23.
//

import Foundation
import Combine

class ViewModel {
    private var model: ResponseModel?
    private var cancellable = Set<AnyCancellable>.init()
    private let output = PassthroughSubject<Output, Never>.init()
    
    enum Input {
        case viewDidLoad
        case retryRequest
    }
    
    enum Output {
        case startLoading
        case fetchDataSuccess(_ response: ResponseObject)
        case fetchDataError(_ error: Error)
    }
    
    func tranfrom(input: PassthroughSubject<Input, Never>) -> AnyPublisher<Output, Never> {
        input
            .sink { [weak self] event in
                switch event {
                case .viewDidLoad:
                    self?.fetchData()
                case .retryRequest:
                    self?.fetchData()
                }
            }
            .store(in: &cancellable)
        return output.eraseToAnyPublisher()
    }
    
    private func fetchData() {
        output.send(.startLoading)
        let urlRequest = URLRequest(url: URL(string: "https://wv-interview.web.app/resource/data.json")!)
        NetworkingService.shared.fetchData(with: urlRequest, responseType: ResponseModel.self) { [weak self] result in
            switch result {
            case .success(let response):
                if response.code == 200 {
                    self?.output.send(.fetchDataSuccess(response.responseObject))
                } else {
                    let apiError = NetworkingError.apiError(response.message)
                    self?.output.send(.fetchDataError(apiError))
                }
            case .failure(let error):
                self?.output.send(.fetchDataError(error))
            }
        }
    }
}
